(function ($) {
    window.onload = function () {
        $(document).ready(function () {		
            stuck_header();
			back_to_top();
			moblie_bar();
            banner_home_page();
            field_ac_click();
            filter_marry();  
            open_search_hd();
            wow = new WOW(
                {
                  animateClass: 'animated',
                  offset: 100
                }
              );
            wow.init();
        });               
    };
})(jQuery);


function back_to_top() {
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 700,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');
	//hide or show the "back to top" link
	$(window).scroll(function () {
		($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if ($(this).scrollTop() > offset_opacity) {
			$back_to_top.addClass('cd-fade-out');
		}
	});
	//smooth scroll to top
	$back_to_top.on('click', function (event) {
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0,
		}, scroll_top_duration
		);
	});
}

function moblie_bar() {
    var $main_nav = $('#main-nav');
    var $toggle = $('.toggle');

    var defaultData = {
        maxWidth: false,
        customToggle: $toggle,
        // navTitle: 'All Categories',
        levelTitles: true,
        pushContent: '#container'
    };

    // add new items to original nav
    $main_nav.find('li.add').children('a').on('click', function() {
        var $this = $(this);
        var $li = $this.parent();
        var items = eval('(' + $this.attr('data-add') + ')');

        $li.before('<li class="new"><a>' + items[0] + '</a></li>');

        items.shift();

        if (!items.length) {
            $li.remove();
        } else {
            $this.attr('data-add', JSON.stringify(items));
        }

        Nav.update(true);
    });

    // call our plugin
    var Nav = $main_nav.hcOffcanvasNav(defaultData);

    // demo settings update

    const update = (settings) => {
        if (Nav.isOpen()) {
            Nav.on('close.once', function() {
                Nav.update(settings);
                Nav.open();
            });

            Nav.close();
        } else {
            Nav.update(settings);
        }
    };

    $('.actions').find('a').on('click', function(e) {
        e.preventDefault();

        var $this = $(this).addClass('active');
        var $siblings = $this.parent().siblings().children('a').removeClass('active');
        var settings = eval('(' + $this.data('demo') + ')');

        update(settings);
    });

    $('.actions').find('input').on('change', function() {
        var $this = $(this);
        var settings = eval('(' + $this.data('demo') + ')');

        if ($this.is(':checked')) {
            update(settings);
        } else {
            var removeData = {};
            $.each(settings, function(index, value) {
                removeData[index] = false;
            });

            update(removeData);
        }
    });
}

function banner_home_page(){
	$('#home-page .banner .owl-carousel').owlCarousel({
		loop:true,
		margin:0,
		nav:true,
		dots: true,
		autoplay:true,
		autoplayTimeout:6000,
		autoplayHoverPause:true,
		responsiveClass:true,
		responsive:{
		0:{
			items:1,
		},
		576:{
		   items:1,
		},
		768:{
		   items:1,
		},
		1024:{
			items:1,
		},
		1400:{
		   items:1,
		   
		}
	 }
  })
}

function field_ac_click(){
    var click_field = document.querySelectorAll('.field-of-activity .row.has-click-elements .col-12');
    var click_field_two = document.querySelectorAll('.field-of-activity .row.has-click-elements .col-12 .activity-imgages');
    if(click_field.length == 0 && click_field_two.length == 0){
        return 0;
    }

    for(var i = 0; i<click_field.length; i++){
        click_field[i].addEventListener('click', function(){
            for(var k = 0; k<click_field.length; k++){
                click_field[k].classList.toggle('d-none');
            }       
            
            this.classList.toggle('col-sm-12');
            this.classList.toggle('col-md-12');
            this.classList.toggle('col-lg-12');     
            this.classList.toggle('d-block');
            for(var j = 0; j<click_field_two.length; j++){
                click_field_two[j].classList.toggle('one-w');
            }


        })
    }

    
}

function filter_marry(){
    var $grid = $('.grid').isotope({
        itemSelector: '.grid-item',
        percentPosition: true,
        
    });
    var $stamp = $grid.find('.stamp');
    $grid.isotope( 'stamp', $stamp );
    $grid.isotope('layout');
    var isStamped = false;

    $('.stamp-button').on( 'click', function() {
    if ( isStamped ) {
        $grid.isotope( 'unstamp', $stamp );
        } else {
        $grid.isotope( 'stamp', $stamp );
        }
    // trigger layout
    $grid.isotope('layout');
    // set flag
    });
    
    // filter functions
    var filterFns = {
        // show if number is greater than 50
        numberGreaterThan50: function() {
        var number = $(this).find('.number').text();
        return parseInt( number, 10 ) > 50;
        },
        // show if name ends with -ium
        ium: function() {
        var name = $(this).find('.name').text();
        return name.match( /ium$/ );
        }
    };
    
    // bind filter button click
    $('#filters').on( 'click', 'button', function() {
        var filterValue = $( this ).attr('data-filter');
        // use filterFn if matches value
        filterValue = filterFns[ filterValue ] || filterValue;
        $grid.isotope({ filter: filterValue });
    });
    
    // bind sort button click
    $('#sorts').on( 'click', 'button', function() {
        var sortByValue = $(this).attr('data-sort-by');
        $grid.isotope({ sortBy: sortByValue });
    });
    
    // change is-checked class on buttons
    $('.button-group').each( function( i, buttonGroup ) {
        var $buttonGroup = $( buttonGroup );
        $buttonGroup.on( 'click', 'button', function() {
        $buttonGroup.find('.is-checked').removeClass('is-checked');
        $( this ).addClass('is-checked');
        });
    });
}

function stuck_header(){
    var top_bar = document.querySelector('#header .top-bar');
    var header_main = document.querySelector('#header .header-main');
    var check = true;

    window.addEventListener('scroll', function(){
        if(window.pageYOffset > 260){
            if(check == true){
                top_bar.classList.add('none-scroll');
                header_main.classList.add('stuck-hd');
                check = false;
            }
        }
        else{
            if(check == false){
                top_bar.classList.remove('none-scroll');
                header_main.classList.remove('stuck-hd');
                check = true;
            }
        }
    })
}

function open_search_hd(){
	var btn_op_search = document.querySelectorAll('#btn-search-hd');
	var over_lay_search = document.querySelector('.over-lay-mb');
    var box_search_hd = document.querySelector('.box-search_header');
	for(var i = 0; i<btn_op_search.length; i++){
		btn_op_search[i].onclick = function(){
			over_lay_search.classList.add('overlay-open');
			box_search_hd.classList.add('active_show');
		}
	}
	over_lay_search.onclick = function(){
		over_lay_search.classList.remove('overlay-open');
		box_search_hd.classList.remove('active_show');
	}
}


